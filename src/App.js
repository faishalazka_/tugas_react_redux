import { ListKontak } from "./components";

function App() {
  return (
    <div>
      <h2>Aplikasi Kontak </h2>
      <hr/>
      <ListKontak />
    </div>
  );
}

export default App;
